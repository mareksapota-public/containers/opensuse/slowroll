# Containers/openSUSE/Slowroll

Unofficial base image for [openSUSE Slowroll][opensuse-slowroll] since at the
moment no official images are available.  This image comes with the same set of
pre-installed software as the official [openSUSE Tumbleweed
image][opensuse-tumbleweed-image].

## Example usage

You can use [podman][podman] to run the container like this:

```
podman run --rm -it registry.gitlab.com/mareksapota-public/containers/opensuse/slowroll:latest
```

While the example uses [podman][podman], other OCI compatible runtimes can be
used to execute the image.

## Updates

This image auto updates on a [weekly schedule][ci-schedule] based on the latest
available [opensuse/tumbleweed:latest][opensuse-tumbleweed-image] image.

# Notice

This image is unofficial and not associated with the [openSUSE][opensuse] project.

As with many OCI images, this image contains software under various licenses
(such as any packages from the base distribution and/or indirect dependencies of
the primary software being contained).  This image follows openSUSE guidelines
for [accepted licenses][opensuse-accepted-licenses] and [format
restrictions][opensuse-restricted-formats].  For a full list of licenses for
software included in this image run `zypper licenses` inside of the container.
As for any pre-built image usage, it is the image user's responsibility to
ensure that any use of this image complies with any relevant licenses for all
software contained within.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[opensuse]: https://www.opensuse.org/
[opensuse-slowroll]: https://en.opensuse.org/openSUSE:Slowroll
[opensuse-accepted-licenses]: https://en.opensuse.org/openSUSE:Accepted_licences
[opensuse-restricted-formats]: https://en.opensuse.org/Restricted_formats
[ci-schedule]: https://gitlab.com/mareksapota-public/containers/opensuse/slowroll/-/pipeline_schedules
[opensuse-tumbleweed-image]: https://registry.opensuse.org/cgi-bin/cooverview?srch_term=project%3D%5EopenSUSE%3AContainers%3A+container%3D%5Eopensuse%2Ftumbleweed%24
[podman]: https://podman.io/
